import React from 'react';
import { Button, Form } from 'react-bootstrap';
import axios from 'axios';
import  { withRouter } from 'react-router-dom';
import { Config } from '../config.js';

class Login extends React.Component {
	
	// Constructor
	constructor(props) {
		super(props);
		
		this.onRequestToken = this.onRequestToken.bind(this);
		this.onUsernameChange = this.onUsernameChange.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);

		this.state = { 
			username: null,
			password: null
		};
	}

	// Username input onchange
	onUsernameChange(e) {
		this.setState({ username: e.target.value });
	}

	// Password input onchange
	onPasswordChange(e) {
		this.setState({ password: e.target.value });
	}

	// Get JWT
	onRequestToken(e) {
		e.stopPropagation();
		
		var bodyFormData = new FormData();
		bodyFormData.set('grant_type', 'password');
		bodyFormData.set('username', this.state.username);
		bodyFormData.set('password', this.state.password);

		axios.request({
			url: "/oauth/token",
			method: "post",
			baseURL: Config.baseUrl,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
			},
			auth: {
				username: "demo",
				password: "secret"
			},
			data: bodyFormData
		}).then((res) => {
			localStorage.setItem("access_token", res.data.access_token);
			this.props.history.push('/dashboard');
		}).catch((error) => {
			localStorage.clear();
			alert("The username and password are incorrect.");
		});
	}

	// Render
	render() {
		return (            
			<Form className="mt-5">
				<h1>Login</h1>

				<Form.Group controlId="formBasicEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" onChange={this.onUsernameChange}/>
					<Form.Text className="text-muted">
						Please enter: <b>admin@admin.com</b> or <b>user@user.com</b>
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="formBasicPassword">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Password" onChange={this.onPasswordChange}/>
					<Form.Text className="text-muted">
						Please enter: <b>1234</b>
					</Form.Text>
				</Form.Group>
				
				<Button variant="primary" type="button" size="sm" onClick={this.onRequestToken}>
					Submit
				</Button>
			</Form>
		);
	}
}

export default withRouter(Login);