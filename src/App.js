import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Login from './Login/Login';
import Dashboard from './Dashboard/Dashboard';
import { Container, Row, Col } from 'react-bootstrap';

function App() {
  return (
    <Router>
      <Container>
        <Row className="justify-content-md-center">
          <Col md={10} lg={8}>
            <Route exact path="/" component={Login} />
            <Route path="/dashboard" component={Dashboard} />
          </Col>
        </Row>
      </Container>
    </Router>
  );
}

export default App;