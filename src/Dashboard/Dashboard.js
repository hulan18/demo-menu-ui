import React from 'react';
import { Container, Navbar, Nav, Button, Table, Modal, Form} from 'react-bootstrap';
import axios from 'axios';
import  { withRouter } from 'react-router-dom';
import { FaTrash } from 'react-icons/fa';
import { Config } from '../config.js';

class Dashboard extends React.Component {

	// Constructor
	constructor(props) {
		super(props);
		
		this.onLogout = this.onLogout.bind(this);
		this.onAddMenu = this.onAddMenu.bind(this);
		this.onCloseModal = this.onCloseModal.bind(this);
		this.onShowModal = this.onShowModal.bind(this);
		this.onMenuNameChange = this.onMenuNameChange.bind(this);
		this.onDeleteMenu = this.onDeleteMenu.bind(this);

		this.state = { 
			menus: [],
			showModal: false,
			menuName: null
		};
	}

	// Logout
	onLogout(e) {
		e.stopPropagation();

		localStorage.clear();
		this.props.history.push('/');
	}

	// Add new menu
	onAddMenu(e) {
		e.stopPropagation();

		let menu = {
			id: null,
			name: this.state.menuName,
			users:[]
		}

		axios.request({
			url: "api/menu",
			method: "post",
			baseURL: Config.baseUrl,
			headers: {
				'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
				'Content-Type': 'application/json'
			},
			data: menu,
		}).then((res) => { 
			if (res.data === true) {
				this.loadMenu();
			}
			else {
				alert("Duplicated menu name.");
			}
		}).catch((error) => { 
			let errObj = JSON.parse(JSON.stringify(error));
			
			if (errObj.response.data.apierror) {
				alert(errObj.response.data.apierror.status);
			}
		});

		this.setState({ showModal: false });
	}

	// Delete menu
	onDeleteMenu(e) {
		e.stopPropagation();

		axios.request({
			url: "api/menu/" + e.currentTarget.value,
			method: "delete",
			baseURL: Config.baseUrl,
			headers: {
				'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
				'Content-Type': 'application/json'
			}
		}).then((res) => {
			if (res.data === true) {
				this.loadMenu();
			}
			else {
				alert("Could not delete menu.");
			}
		}).catch((error) => { 
			let errObj = JSON.parse(JSON.stringify(error));
			
			if (errObj.response.data.apierror) {
				alert(errObj.response.data.apierror.status);
			}
		});
	}

	// Close menu  modal
	onCloseModal() {
    this.setState({ showModal: false });
  }

	// Show menu modal
  onShowModal() {
    this.setState({ showModal: true });
	}
	
	// Menu name input onchange
	onMenuNameChange(e) {
		this.setState({ menuName: e.target.value });
	}

	// Did mount
	componentDidMount() {
		this.loadMenu();
	}

	loadMenu() {
		axios.request({
			url: "api/menu",
			method: "get",
			baseURL: Config.baseUrl,
			headers: {
				'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
				'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
			}
		}).then((res) => {
			this.setState({ menus: res.data });
		}).catch((error) => {
			alert("Something went wrong.");
		});
	}

	// Render
	render() {
		return (
			<Container>
				<Navbar bg="primary" variant="dark">
					<Navbar.Brand>Dashboard</Navbar.Brand>
					<Nav className="mr-auto">
					</Nav>
					<Nav>
						<Button onClick={this.onLogout}>Logout</Button>
					</Nav>
				</Navbar>

				<h4 className="mt-5">Menu</h4>

				<Table striped bordered   size="sm">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Allowed users</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{this.state.menus.map((menu, i) => 
							<tr key={menu.id}>
								<td>{i+1}</td>
								<td>{menu.name}</td>
								<td>
									{menu.users.map((user, j) => user.email )}
								</td>
								<td className="text-center"><Button value={menu.id} onClick={this.onDeleteMenu} variant="link" size="xs" className="p-0"><FaTrash size=".8em"/></Button></td>
							</tr>
						)}
					</tbody>
				</Table>

				<Button onClick={this.onShowModal} size="sm">Add menu</Button>

				<Modal show={this.state.showModal} onHide={this.onCloseModal}>
          <Modal.Header closeButton>
            <Modal.Title>Add Menu</Modal.Title>
          </Modal.Header>
          <Modal.Body>
						<Form>
							<Form.Group controlId="formBasicEmail">
								<Form.Label>Name</Form.Label>
								<Form.Control type="text" placeholder="Enter name" onChange={this.onMenuNameChange} />
							</Form.Group>
						</Form>
					</Modal.Body>
          <Modal.Footer>
            <Button size="sm" variant="secondary" onClick={this.onCloseModal}>
              Close
            </Button>
            <Button size="sm" variant="primary" onClick={this.onAddMenu}>
              Save
            </Button>
          </Modal.Footer>
        </Modal>
			</Container>
		);
	}
}

export default withRouter(Dashboard);